
/* Arrays: Lists of values
   ------------------------------------------------------------------------- */

// Values are separated by comas. They can be of any type.
var friends = ['Juanjo', 'Luz', 'Chris', 'Juancho', [true, 0, 1]];
console.log(friends);

// Arrays have some properties we can access
console.log(friends.length);

// A value can be recalled passing its index to the array
var friendNumber = 3;
console.log(friends[friendNumber]) // => Juancho

// Arrays can be traversed by with a for loop, using the counter as index
for (var i = 0; i < friends.length; i++) {
  console.log(friends[i]);
};

/* Objects: Flexible way to store values with named properties
   ------------------------------------------------------------------------- */

var me = {
  first_name: 'Dave',
  last_name:  'Rojo',
  'Employee Number': 1 // complex attribute name
};

console.log(me);
console.log(me.first_name);
console.log(me.last_name);
console.log(me['Employee Number']);
