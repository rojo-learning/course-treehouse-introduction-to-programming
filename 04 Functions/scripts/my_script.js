
/* Functions: Sets of instructions callable in diferent parts of the code.
   ------------------------------------------------------------------------- */

var sayHello = function() {
  var message = "Hello";
  message += " World";
  console.log(message);
};

var debug = function(message) {
  console.log(message);
};

var doubleNumber = function(number) {
  return number * 2;
};

debug(doubleNumber(7));

sayHello();

// Do something
var x = 1;
debug('x has been set');

sayHello();

// Do something else
x += 10;
var y = 100;

debug("x value increased, y value set");

sayHello();
