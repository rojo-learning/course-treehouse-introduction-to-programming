
/* Control Flow: If/Else
   ------------------------------------------------------------------------- */
// Dynamically determine if an instruction will be executed or not.

// True values: Whatever not false, haha!
console.log('Before');
if (true) {
  // Gets in if the condition is true
  console.log('If Block');
} else {
  // As the condition above meet, it won't get in
  console.log('Else Block');
};
console.log('After');

// False values: false, 0, "", null (nothing), undefined (not declared)
console.log('Before');
if (false) {
  // If the condition is false, it won't get in
  console.log('If Block');
} else {
  // As the condition above did not meet, it gets in
  console.log('Else Block');
};
console.log('After');

/* Control Flow: Cycles
   ------------------------------------------------------------------------- */
// Execute a set of instructions over and over again.

// While executes an instruction while a condition is meet (evaluates true).
// Care should be taken of not to fall into infinite loops!
console.log('Before While');

var counter = 10;
while (counter) {
  console.log('Hello, World!');
  counter= counter - 1;
}

console.log('After While');

// For executes an instruction for a set number or times.
console.log('Before For');

for (var counter = 10; counter ; counter = counter - 1) {
  console.log('Hello, World!');
};

console.log('After For');
