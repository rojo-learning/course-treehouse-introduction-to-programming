
/* Writting to the JavaScript Console
   ------------------------------------------------------------------------- */

console.log("Hello from my_first_script.js");
console.log("Hello again!");


/* Variables
   ------------------------------------------------------------------------- */

// Variable declaration ans assignment
var name = prompt("What is your name?");
// Use of the variable contents
alert("Hello " + name);
// Changing the value the variable points to
name = "Jim";
console.log("The user's name is " + name);
