/* Arithmetic
   ------------------------------------------------------------------------- */
2 + 7   // => 9
12 * 24 // => 288

/* Strings
   ------------------------------------------------------------------------- */
// Single and double quoted strings
"Hello" // => "Hello"
'Hi'    // => "Hi"

// String contatenation
"Hello" + "World"  // => "HelloWorld"
"Hello " + "World" // => "Hello World"

/* Functions
   ------------------------------------------------------------------------- */
// Data output (shows an alert window in the browser)
alert(12)            // => undefined
alert("Hello World") // => undefined

// Data input (shows an input window in hte browser)
prompt("What is your name?") // => "user input as a string"

// Combining functions (prompts for value and displays a menssage)
alert("Hi, " + prompt("What is your name?")) // => undefined
