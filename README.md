# Treehouse - Introduction to Programming #

This repository contains practice examples from Introduction to Programming with Javascript, imparted by Jim Hoskins at Threehouse.

> Basic programming skills are essential for all web professionals, including designers. Many programming languages share a common set of concepts, which will be covered in this course.

## Contents ##

- **Basics**: Computer programming can be intimidating at first, but this gentle introduction to the subject will make the learning curve a bit easier to climb. _This section contains examples with the use of arithmetics, strings, functions, variables and comments._
- **Control Structures**: Control structures determine the flow of execution within an application. A conditional statement can fork the execution down several paths or a loop can execute the same code several times until a condition is met.
- **Objects and Arrays**: An array is a way of storing multiple items and each item is associated to a number called the index. Each item can be accessed using the index value. Objects are usually associated to meaningful pieces of data which it can hold and manipulate.
- **Functions**: A function contains a piece of code that needs to be executed several times from different parts of your application. A function optionally takes in arguments and returns an object or value as a result.

##Extra Credits##

- Try out different variable names. See which variable names work, and which ones don't.
- Write a program that loops through the numbers 1 through 100. Each number should be printed to the console, using console.log(). However, if the number is a multiple of 3, don't print the number, instead print the word "fizz". If the number is a multiple of 5, print "buzz" instead of the number. If it is a multiple of 3 and a multiple of 5, print "fizzbuzz" instead of the number.
- Try creating an object that represents you. Include all sorts of things about you, like your name as a string, and your favorite songs as an array. Explore how objects can be nested in other objects, and arrays.
- Create a function that takes 3 arguments, a, b, and c. If a is an even number have the function return the string "even". Otherwise have the function return the string "odd".

---
This repository contains code examples from Treehouse. These are included under fair use for showcasing purposes only. Those examples may have been modified to fit my particular coding style.