
/** Variables
"You use variables as symbolic names for values in your application. The names
of variables, called identifiers, conform to certain rules.

A JavaScript identifier must start with a letter, underscore (_), or dollar sign
($); subsequent characters can also be digits (0-9). Because JavaScript is case
sensitive, letters include the characters "A" through "Z" (uppercase) and the
characters "a" through "z" (lowercase)."

-- Excerpt from Mozilla Developer Network
*/

/** Legan vVariable Names :)

var alpha
var _dog
var $1000
var Terra2
var slush_n_puppies
var whereIsMy$

*/

/** Ilegal Variable Names :(

var 1twothree
var ~wave
var #hashtag
var +plus
var @at
var !not
*/
