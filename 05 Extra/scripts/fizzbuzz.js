// A Fizz Buzz version in JavaScript

var text;

for (var number = 1; number <= 100; number++) {
  text = "";

  if (number % 3 == 0) { text  = "fizz" };
  if (number % 5 == 0) { text += "buzz" };

  if (text) {
    console.log(text);
  } else {
    console.log(number);
  };
};
