
var me = {
  name: 'David',
  surname: 'Rojo',

  birthdate: {
    day: 6,
    month: 8,
    year: 1981
  },

  birthplace: {
    city: 'Celaya',
    state: 'Guanajuato',
    country: 'Mexico'
  },

  residence: {
    adress: '289 Paseo Futurama',
    neighborhood: 'La Pradera',
    city: 'Irapuato',
    stage: 'Guanjauto',
    country: 'Mexico'
  },

  height: 184,
  weigth: 96
};

console.log(me);
