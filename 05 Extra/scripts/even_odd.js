
// Checks the evenness or oddity of the 'a' param.
var check_param = function(a, b, c) {
  if (a % 2 == 0) { return 'even'; };

  return 'odd';
};

console.log(check_param(1,2,3));
console.log(check_param(4,5,6));
